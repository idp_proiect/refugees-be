package pweb.refugees.enums;

public enum BookingStatus {
    PENDING,
    APPROVED,
    REJECTED;
}
