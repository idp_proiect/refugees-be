package pweb.refugees.enums;

public enum Role {
    ROLE_GUEST,
    ROLE_HOST,
    ROLE_USER;
}
